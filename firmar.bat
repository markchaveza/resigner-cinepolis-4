cls
@echo off

SET PATH_APK=./app/build/outputs/apk
REM SET JAR_SIGNER_PATH_=C:\android-studio\jre\bin\
REM SET JAR_SIGNER_PATH=C:\Program Files\Java\jdk1.8.0_131\bin


set APK_VERSION=4.1.5.8
set APK_ENVIROMENT=beta



set APK_ORIGNAL_FILE=Cinepolis-%APK_VERSION%-android-%APK_ENVIROMENT%.apk
set APK_SIGNED_FILE=Cinepolis-%APK_VERSION%-%APK_ENVIROMENT%.apk
set CERT_FILE=CinepolisDist.keystore
set KEY_ALIAS=cinepolisdist
set KEY_STORE_PASSWORD=developer
set KEY_PASSWORD=ia



REM Cinepolis-4.1.1.3-stage-debug
echo APK_ORIGNAL_FILE:  %APK_ORIGNAL_FILE%
echo APK_SIGNED_FILE:  %APK_SIGNED_FILE%
echo CERT_FILE:  %CERT_FILE%

pause
echo.......................................REMOVER META-INF.......................................

zip -d %APK_ORIGNAL_FILE% "META-INF*"

pause
echo .......................................FIRMAR APK.......................................

"./bin/jarsigner.exe " -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore %CERT_FILE% %APK_ORIGNAL_FILE% cinepolisdist -storepass developer -keypass ia

pause
echo .......................................VERIFICAR APK.......................................

"./bin/jarsigner.exe " -verify -verbose -certs %APK_ORIGNAL_FILE% 
pause
echo .......................................ZIPALIGN APK.......................................

zipalign -v 4  %APK_ORIGNAL_FILE%  %APK_SIGNED_FILE%